var content = document.getElementById('wrap-post-f'), // mengambil element dengan id wrap-post pada DOM
	wrapFeed = document.getElementById('wrap-feed'); // mengambil element dengan id wrap-feed pada DOM

window.onscroll = function() {
	// ambil elemen pada DOM lalu masukkan kedalam variabel
	var h = document.documentElement, 
        b = document.body,
        st = 'scrollTop',
        sh = 'scrollHeight';

    var percent = (h[st]||b[st]) / ((h[sh]||b[sh]) - h.clientHeight) * 100;
	
	if (percent >= 90) { // jika scroll berada diatas posisi 90 persen maka
		content.style.position = 'relative'; // mengubah posisi menjadi relative pada object content
		content.style.paddingTop = 0; // mengubah padding-top menjadi 0 pada object content

		wrapFeed.style.paddingBottom = 0; // mengubah padding-bottom menjadi 0 pada object wrapFeed
	} else {
		content.style.position = 'fixed'; // mengubah posisi menjadi fixed pada object content
		content.style.bottom = 0; // mengubah posisi agar dibawah pada object content
		wrapFeed.style.paddingBottom = '250px'; // mengubah nilai padding-bottom menjadi 250px pada object wrapFeed
	}
};