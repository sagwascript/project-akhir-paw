<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Atemmo</title>
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
	<div id="wrapper">
		<div id="boundary"></div>
		<?php
			session_start();
			
			$footer = false;
			if (isset($_GET['page'])) { // jika terdapat variabel page pada url maka

				$available_pages = ['login', 'register', 'profile', 'main', 'edit_profile']; // definisikan halaman yang dapat dikunjungi

				$page = $_GET['page']; // ambil nilai dari variabel page 
				
				if (isset($_SESSION['loggedin'])) { // jika session loggedin bernilai true maka

					if (in_array($page, $available_pages)) { // cek pada array available_page jika ada maka

						if (isset($_GET['action'])) { // cek jika di url terdapat variabel action, jika ada maka

							if ($page == 'profile' AND $_GET['action'] == 'edit') { // jika page berisi profile dan action berisi edit maka
								$page = 'edit_profile'; // variabel page diisikan dengan edit_profile
							}
						}
						include('pages/'.$page.".php"); // includekan sesuai dengan variabel page dengan diberikan akhiran .php
						$footer = true;	 // set footer menjadi true
					} else { // jika halaman yang dituju tidak ada didalam available_pages maka 
						include 'pages/main.php'; // include main.php
						$footer = true;	 // set footer menjadi true
					}
				} else { // jika user belum login maka

					if ($page == 'register') { // jika page pada url adalah register maka
						include('pages/register.php'); // include halaman register.php
					} else { // jika tidak maka
						include('pages/login.php'); // include halaman login.php
					}

				}

			} else { // jika user url tidak mengandung variabel page maka

				if (isset($_SESSION['loggedin'])) {
					if ($_SESSION['loggedin']['status']) {
						$footer = true;	 // set footer menjadi true		
					}
				}
				
				include('app/auth.php'); // include file auth.php
			}

		?>
	</div>
	<div style="clear: both;"></div>
	<?php
		if($footer) { // jika variabel footer bernilai true maka tampilkan elemen footer
	?>
		<footer>
			<p>Copyright &copy; by Kelompok PAW-B03</p>
		</footer>
	<?php 
		} 	
	?>
</body>
</html>