<?php

			include 'components/nav.inc'; // include elemen navigasi yang terletak di folder components

			include 'app/koneksi.php'; // include file koneksi.php untuk koneksi database


			$account = true; // set variabel account true

			if (isset($_GET['username'])) { // jika terdapat variabel username pada url
				$username_teman = $_GET['username']; // ambil nilai dari varabel username
				
				$q = $connection->prepare("SELECT * FROM users INNER JOIN friends ON users.username = friends.username_teman WHERE friends.username = :username AND friends.username_teman = :username_teman"); // select semua data user yang berteman dengan user yang login
				$q->bindValue(':username', $_SESSION['loggedin']['username']); // ganti parameter :username dengan variabel dari session
				$q->bindValue(':username_teman', $username_teman); // ganti parameter :username_teman dengan variabel $username_teman
				$q->execute(); // eksekusi query sql

				if ($q->rowCount() == 0) {
					redirect('index.php?page=profile');
				} else {
					$account = false; // set variabel account menjadi false
				}


			} else { // jika tidak terdapat variabel username pada url maka

				$q = $connection->prepare("SELECT * FROM users WHERE username = :username"); // select tabel user yang nama usernamenya adalah :username
				$q->bindValue(':username', $_SESSION['loggedin']['username']); // ganti parameter :username dengan nilai dari Session
				$q->execute(); // eksekusi sql

			}
			
			$res = $q->fetch(); // ambil data pertama dari hasil eksekusi sql

			if (count($res) > 1) { // jika panjang dari variabel res lebih besar dari 1 artinya data ada

?>
			<div id="left" style="width: 35%;">
				<div id="sidebar-profil">
			
				<!-- Tampilkan profil sesuai dengan hasil eksekusi SQL -->
				<span>Profil</span>
				<div id="wrap-profil">
					<div class="table" style="padding-bottom: 30px;">
						<div class="row">
							<div class="col md">Nama Depan</div>
							<div class="col md">: <?php echo $res['nama_depan']; ?></div>
						</div>
						<div class="row">
							<div class="col md">Nama Belakang</div>
							<div class="col md">: <?php echo $res['nama_belakang']; ?></div>
						</div>
						<div class="row">
							<div class="col md">Email</div>
							<div class="col">: <?php echo $res['email']; ?></div>
						</div>
						<div class="row">
							<div class="col md">Tanggal Lahir</div>
							<div class="col md">: <?php echo $res['tanggal_lahir']; ?></div>
						</div>
						<div class="row">
							<div class="col md">Jenis Kelamin</div>
							<div class="col md">: <?php echo $res['jenis_kelamin'] == 'L' ? 'Laki-laki' : 'Perempuan'; ?></div>
						</div>
						<div class="row">
							<div class="col lg">Alamat : </div>
						</div>
						<div class="row">
							<div class="col lg">
								<?php echo $res['alamat']; ?>
							</div>
						</div>
						<?php if ($account) {  // jika variabel account bernilai true berarti profil yang dilihat adalah profil username itu sendiri maka tampilkan tombol edit?>
						<div class="row">
							<div class="col" style="width: 90%;">
								<a href="index.php?page=profile&action=edit" class="button green" style="float: right;">Edit Profil</a>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>

			<div id="sidebar-teman">
				<span>Daftar Teman</span>
				<div id="wrap-teman">
					<?php
						include('app/koneksi.php'); // include koneksi.php untuk menyambungkan dengan database

						$username = $_SESSION['loggedin']['username']; // set variabel username dengan nilai dari session

						if (isset($_GET['username'])) { // jika pada url terdapat variabel username maka
							$username = $_GET['username']; // isikan variabel username dengan nilai pada url
						}

						$q = $connection->prepare("SELECT * FROM friends WHERE username = :username"); // select semua teman dari user
						$q->bindValue(':username', $username); // ganti parameter :username dengan variabel username
						$q->execute(); // eksekusi SQL

						if ($q->rowCount() < 1) {
					?>
							<div class="ind-user">
								<div class="username">
									<a href="#">
										Tidak memiliki teman.
									</a>
								</div>
							</div>
					<?php		
						} else {
							foreach ($q as $res) { // tampilkan semua hasil dari eksekusi SQL
					?>
								<div class="ind-user">
									<div class="username">
										<a href="?page=profile&username=<?php echo $res['username_teman'] ?>">
											<?php echo $res['username_teman']; ?>
										</a>
									</div>
								</div>
					<?php
							}
						}
					?>
					</div>
				</div>
			</div>
	
			<div id="content" style="width: 65%;height: 100%;">
			
				<div id="wrap-feed">
					<h2>Kiriman</h2>
					<?php

						include 'app/koneksi.php'; // include koneksi.php pada direktori app untuk menyambungkan dengan database

						$username = $_SESSION['loggedin']['username']; // set variabel username dengan nilai dari variabel session

						if (isset($_GET['username'])) { // jika terdapat variabel username pada url maka 
							$username = $_GET['username']; // isikan variabel username dengan nilai dari variabel pada username menggunakan GET
						}

						$q = $connection->prepare("SELECT * FROM posts WHERE username = :username"); // select semua post yang dibuat oleh username pada variabel username
						$q->bindValue(':username', $username);
						$q->execute(); // eksekusi SQL

						foreach ($q as $res) { // tampilkan semua data hasil eksekusi SQL
							
					?>
						<div class="feed">
							<div class="user-post">
								<?php echo $res['username']; ?>
							</div>
							<p>
								<?php echo $res['isi']; ?>
							</p>
							<span class="post-date">
								<?php echo $res['tanggal']; ?>
							</span>
						</div>

					<?php

						}

					?>
				</div>
			</div>

		<?php

			}

		?>