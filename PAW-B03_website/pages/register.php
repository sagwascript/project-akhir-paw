<?php
	if (isset($_SESSION['loggedin'])) { // cek apakah user sudah login ketika mengakses halaman ini
		include('pages/main.php'); // jika sudah login maka tampilkan halaman main.php
	} else { // jika tidak maka tampilkan semua halaman diblok ini

?>
<div class="register">
	<h1>Daftar</h1>
	<hr>
	<?php

		include 'app/register.php';

		if (isset($_SESSION['register'])) { // cek jika terdapat session register
			if ($_SESSION['register']['status']) {
				$_SESSION['register']['status'] = false; // jika session error sudah ditampilkan sekali maka set menjadi false
			} else { // jika false maka session error di destroy agar tidak menampilkan pesan error
				unset($_SESSION['register']);
			}
		}

		function val($field) { // fungsi untuk menampilkan nilai dari field yang sudah benara / lolos validasi
			if (isset($_SESSION['register'])) {
				if (!isset($_SESSION['register']['errors'][$field])) {
					echo $_SESSION['register']['data'][$field]; // menampilkan nilai dari field yang disimpan pada session
				}
			}
		}
	?>
	<form action="index.php?page=register" method="post">
		<div class="table">
			<?php
				if (isset($_SESSION['register']['errors']['tahun'])) { // cek jika terdapat session error untuk field tahun
					foreach ($_SESSION['register']['errors']['tahun'] as $value) { // tampilkan errornya
			?>
						<div class="row">
							<div class="col lg" style="color:red;"><?php echo "* ".$value."</br>"; ?></div> 
						</div>
			<?php
					}
				}
			?>
			<div>
				<div class="col md">
					<label>Username</label>			
				</div>
				<div class="col md">
					: <input type="text" name="username" class="input" value="<?php val('username'); ?>">
				</div>
				<div class="col md error-msg">
					<?php
						if (isset($_SESSION['register']['errors']['username'])) { // cek jika terdapat error pada field username
							foreach ($_SESSION['register']['errors']['username'] as $value) { // tampilkan semua errornya
								echo "* ".$value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Password</label>
				</div>
				<div class="col md">
					: <input type="password" name="password" class="input" value="<?php val('password'); ?>">
				</div>
				<div class="col md error-msg">
					<?php
						if (isset($_SESSION['register']['errors']['password'])) { // cek jika terdapat error pada field password
							foreach ($_SESSION['register']['errors']['password'] as $value) { // tampilkan semua errornya
								echo "* ".$value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Konfirmasi Password</label>
				</div>
				<div class="col md">
					: <input type="password" name="re-password" class="input" value="<?php val('re-password'); ?>">			
				</div>
				<div class="col md error-msg">
					<?php
						if (isset($_SESSION['register']['errors']['re-password'])) { // cek jika terdapat error pada field re-password
							foreach ($_SESSION['register']['errors']['re-password'] as $value) { // tampilkan semua errornya
								echo "* ".$value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Email</label>
				</div>
				<div class="col md">
					: <input type="text" name="email" class="input" value="<?php val('email'); ?>">			
				</div>
				<div class="col md error-msg">
					<?php
						if (isset($_SESSION['register']['errors']['email'])) { // cek jika terdapat error pada field email
							foreach ($_SESSION['register']['errors']['email'] as $value) { // tampilkan semua errornya
								echo "* ".$value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md"> 
					<label>Nama Depan</label>			
				</div>
				<div class="col md">
					: <input type="text" name="nama_depan" class="input" value="<?php val('nama_depan'); ?>">			
				</div>
				<div class="col md error-msg">
					<?php
						if (isset($_SESSION['register']['errors']['nama_depan'])) { // cek jika terdapat error pada field nama_depan
							foreach ($_SESSION['register']['errors']['nama_depan'] as $value) { // tampilkan semua errornya
								echo "* ".$value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Nama Belakang</label>			
				</div>
				<div class="col md">
					: <input type="text" name="nama_belakang" class="input" value="<?php val('nama_belakang'); ?>">			
				</div>
				<div class="col md error-msg">
					<?php
						if (isset($_SESSION['register']['errors']['nama_belakang'])) {  // cek jika terdapat error pada field nama_belakang
							foreach ($_SESSION['register']['errors']['nama_belakang'] as $value) { // tampilkan semua errornya
								echo "* ".$value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Tanggal Lahir</label>			
				</div>
				<div class="col md">
					: <input type="date" class="date" name="tanggal_lahir" value="<?php val('tanggal_lahir'); ?>">			
				</div>
				<div class="col md error-msg">
					<?php
						if (isset($_SESSION['register']['errors']['tanggal_lahir'])) {  // cek jika terdapat error pada field tanggal_lahir
							foreach ($_SESSION['register']['errors']['tanggal_lahir'] as $value) { // tampilkan semua errornya
								echo "* ".$value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Jenis Kelamin</label> : 			
				</div>
				<div class="col md">
					: <input type="radio" class="radio" name="jenis_kelamin" value="L" checked> Laki-laki 
					<input type="radio" class="radio" name="jenis_kelamin" value="P"> Perempuan					
				</div>
				<div class="col md error-msg">
					<?php
						if (isset($_SESSION['register']['errors']['jenis_kelamin'])) {  // cek jika terdapat error pada pilihan radio button jenis_kelamin
							foreach ($_SESSION['register']['errors']['jenis_kelamin'] as $value) { // tampilkan semua errornya
								echo "* ".$value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Alamat</label>
				</div>
				<div class="col md error-msg">
					<?php
						if (isset($_SESSION['register']['errors']['alamat'])) { // cek jika terdapat error pada field alamat
							foreach ($_SESSION['register']['errors']['alamat'] as $value) { // tampilkan semua errornya
								echo "* ".$value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<textarea name="alamat" cols="80" rows="10"><?php val('alamat'); ?></textarea>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<a href="index.php" class="button red">Kembali</a>
					<input type="submit" name="btnregister" value="Daftar" class="button green">
				</div>
			</div>
		</div>
	</form>	
</div>
<?php

	}

?>