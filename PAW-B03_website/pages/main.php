<?php

	include 'components/nav.inc'; // include elemen navigasi pada direktori components

?>
<div id="left" style="width: 25%;">
	<div id="sidebar">
		<span>Pengguna Lain</span>
		<div id="wrap-users">
			<?php
				include 'app/koneksi.php'; // include koneksi.php untuk menyambungkan dengan database
				include 'app/validator.inc';		

				$q = $connection->prepare("SELECT * FROM users WHERE username != :username AND username NOT IN(SELECT username_teman FROM friends WHERE username = :username)"); // select smeua user yang tidak sama dengan variabel username dan tidak ada didalam list teman dari username
				$q->bindValue(':username', $_SESSION['loggedin']['username']); // ganti parameter :username dengan nilai dari variabel session
				$q->execute(); // eksekusi SQL

				foreach ($q as $res) { // tampilkan semua data dari hasil eksekusi SQL
			?>
					<div class="ind-user">
						<div class="username"><?php echo $res['username'] ?></div>
						<form action="" method="post">
							<input type="hidden" name="username_teman" value="<?php echo $res['username']; ?>">
							<input type="submit" value="Tambah" name="btntambah" class="add-friend">
							<!-- <a href="#" class="add-friend">Tambahkan</a>	 -->
						</form>
					</div>
			<?php
				}
			?>
		</div>
	</div>
</div>

<div id="content">
	
	<div id="wrap-feed">
		<h2>Kiriman</h2>
		<?php

			include 'app/koneksi.php'; // include koneksi.php untuk menyambungkan dengan database 

			$q = $connection->prepare("SELECT * FROM posts WHERE username = :username OR username IN (SELECT username_teman FROM friends WHERE username = :username) ORDER BY tanggal DESC"); // select semua post yang dipostkan oleh username yang sedang login dan post dari semua teman dari usernmae yang login

			$q->bindValue(':username', $_SESSION['loggedin']['username']); // ganti parameter :username dengan nilai dari session
			$q->execute(); // eksekusi SQL

			foreach ($q as $res) { // tampilkan semua data yang didapatkan dari eksekusi SQL
				
		?>
			<div class="feed">
				<div class="user-post">
					<?php echo $res['username'] == $_SESSION['loggedin']['username'] ? 'Anda' : $res['username']; // jika username yang melakukan posting adalah username yang sedang login maka ganti username dengan And ?> 
				</div>
				<p>
					<?php echo $res['isi']; // tampilkan isi dari  post ?>
				</p>
				<span class="post-date">
					<?php echo $res['tanggal']; // tampilkan tanggal ?>
				</span>
			</div>

		<?php

			}

		?>
	</div>

	<?php

		if ($q->rowCount() < 3) {
			$style = 'wrap-post-r';
		} else {
			$style = 'wrap-post-f';
		}

	?>
	<div id="<?php echo $style; ?>">
		<form action="" method="post">
			<h3>Post</h3>
			<div class="table">
				<div class="row">
					<div class="col">
						<textarea name="isi" id="" cols="100" rows="10" style="float: left;"></textarea>
						<input type="submit" value="Post!" name="btnpost" style="float: right; margin-left: 10px; margin-top: 0px;" class="button green">	
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php
	if (!isset($_GET['page']) && $q->rowCount() >= 3) {
?>
		<script src="assets/js/script.js"></script>
<?php 
	} 
?>


<?php

	if (isset($_POST['btnpost'])) { // jika user sudah menekan tombol btnpost maka

		include 'app/koneksi.php'; // include koneksi.php untuk menyambungkan dengan database

		$isi = htmlentities($_POST['isi']); // htmlentities agar inputan user dikonversi menjadi teks biasa dan tidak dianggap sebagai tag html

		$request = $_POST; // mengisikan variabel request dengan variabel $_POST

		$rules = [ // definisikan rules/aturan untuk tiap kolom isian
			'isi' => 'required|min:3'					
		];

		$messages = [ // definisikan pesan error untuk setiap aturan
			'required' => 'Isian wajib diisi',
			'min' => 'Panjang @field minimal @size karakter'
		];

		$errors = validate($request, $rules, $messages); // memanggil fungsi validate

		if (count($errors) < 1) {
			$q = $connection->prepare("INSERT INTO posts (username, isi) VALUES (:username, :isi);"); // isikan post kolom username dan password dengan nilai :username dan :isi
			$q->bindValue(':username', $_SESSION['loggedin']['username']); // ganti parameter :username dengan nilai dari session
			$q->bindValue(':isi', $isi); // ganti parameter :isi dengan variabel isi

			$q->execute(); // eksekusi SQL

			redirect('index.php'); // redirect ke halaman index
		}

	}

	if (isset($_POST['btntambah'])) { // jika user sudah menekan tombol btnpost maka
		include 'app/koneksi.php'; // include koneksi.php untuk menyambungkan dengan database

		$q = $connection->prepare("INSERT INTO friends VALUES (:username, :username_teman)"); // insert pada tabel friends dengan nilai :username dan :username_teman
		$q->bindValue(':username', $_SESSION['loggedin']['username']); // gantikan parameter :username dengan nilai dari session
		$q->bindValue(':username_teman', $_POST['username_teman']); // gantikan parameter :username_teman dengan nilai dari variabel post username_teman
		$q->execute(); // eksekusi SQL

		redirect('index.php'); // redirect ke halaman index
	}

?>