<?php
	
	include 'components/nav.inc'; // include nav.inc untuk menampilkan elemen navigasi web

	include 'app/koneksi.php'; // include koneksi.php untuk menyambungkan dengan database

	if (isset($_SESSION['edit'])) { // jika terdapat session dengan key edit maka
		if ($_SESSION['edit']['status']) { // jika session dengan key edit dan statusnya bernilai true
			$_SESSION['edit']['status'] = false; // jika session error sudah ditampilkan sekali maka set menjadi fals
		} else { // jika false maka session error di destroy agar tidak menampilkan pesan error
			unset($_SESSION['edit']);
		}
	}

	$q = $connection->prepare("SELECT * FROM users WHERE username = :username"); // select semua kolom yang usernamenya adalah :username
	$q->bindValue(':username', $_SESSION['loggedin']['username']); // ganti parameter :username dengan nilai dari session
	$q->execute(); // eksekusi SQL

	$res = $q->fetch(); // ambil data pertama dari hasil eksekusi SQL
?>
<div class="register" style="margin-top: 70px;">
	<h1 class="edit-profil">Edit Profil</h1>
	<hr>
	<form action="" method="post">
		<div class="table">
			<div class="row">
				<div class="col md">
					<label>Username</label>			
				</div>
				<div class="col md">
					: <input type="text" name="username" class="input" value="<?php echo $res['username'] ?>">
				</div>
				<div class="col md">
					<?php
						if (isset($_SESSION['edit']['errors']['username'])) { // cek jika terdapat error pada field username
							foreach ($_SESSION['edit']['errors']['username'] as $value) { // tampilkan semua errornya
								echo $value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Email</label>
				</div>
				<div class="col md">
					: <input type="text" name="email" class="input" value="<?php echo $res['email'] ?>">			
				</div>
				<div class="col md">
					<?php
						if (isset($_SESSION['edit']['errors']['email'])) { // cek jika terdapat error pada field email
							foreach ($_SESSION['edit']['errors']['email'] as $value) { // tampilkan semua errornya
								echo $value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Nama Depan</label>			
				</div>
				<div class="col md">
					: <input type="text" name="nama_depan" class="input" value="<?php echo $res['nama_depan'] ?>">			
				</div>
				<div class="col md">
					<?php
						if (isset($_SESSION['edit']['errors']['nama_depan'])) { // cek jika terdapat error pada field nama_depan
							foreach ($_SESSION['edit']['errors']['nama_depan'] as $value) { // tampilkan semua errornya
								echo $value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Nama Belakang</label>			
				</div>
				<div class="col md">
					: <input type="text" name="nama_belakang" class="input" value="<?php echo $res['nama_belakang'] ?>">			
				</div>
				<div class="col md">
					<?php
						if (isset($_SESSION['edit']['errors']['nama_belakang'])) {  // cek jika terdapat error pada field nama_belakang
							foreach ($_SESSION['edit']['errors']['nama_belakang'] as $value) { // tampilkan semua errornya
								echo $value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Tanggal Lahir</label>			
				</div>
				<div class="col md">
					: <input type="date" name="tanggal_lahir" class="date" value="<?php echo $res['tanggal_lahir'] ?>">			
				</div>
				<div class="col md">
					<?php
						if (isset($_SESSION['edit']['errors']['tanggal_lahir'])) {  // cek jika terdapat error pada field tanggal_lahir
							foreach ($_SESSION['edit']['errors']['tanggal_lahir'] as $value) { // tampilkan semua errornya
								echo $value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Jenis Kelamin</label> 			
				</div>
				<div class="col md">
					: <input type="radio" name="jenis_kelamin" class="radio" value="L" <?php echo $res['jenis_kelamin'] == 'L' ? 'checked' : ''; ?> > Laki-laki 
					<input type="radio" name="jenis_kelamin" class="radio" value="P"<?php echo $res['jenis_kelamin'] == 'P' ? 'checked' : ''; ?> > Perempuan			
				</div>
				<div class="col md">
					<?php
						if (isset($_SESSION['edit']['errors']['jenis_kelamin'])) {  // cek jika terdapat error pada pilihan radio button jenis_kelamin
							foreach ($_SESSION['edit']['errors']['jenis_kelamin'] as $value) { // tampilkan semua errornya
								echo $value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col md">
					<label>Alamat</label>
				</div>
				<div class="col lg">
					<?php
						if (isset($_SESSION['edit']['errors']['alamat'])) { // cek jika terdapat error pada field alamat
							foreach ($_SESSION['edit']['errors']['alamat'] as $value) { // tampilkan semua errornya
								echo $value."</br>";
							}
						}
					?>
				</div>
			</div>
			<div class="row">
				<div class="col lg">
					<textarea name="alamat" id="" cols="80" rows="10"><?php echo $res['alamat']; ?></textarea>
				</div>
			</div>
			<div class="row">
				<div class="col lg">
					<a href="index.php?page=profile" class="button red">Kembali</a>
					<input type="submit" name="btnsimpan" value="Simpan" class="button green">
				</div>
			</div>
		</div>
	</form>
	<?php
		include 'app/edit_profile.php';
	?>
</div>