<?php

	if(!isset($_SESSION['loggedin']['status'])) { // jika session loggedin statusnya adalah false maka
		include('pages/login.php'); // tampilkan halaman login.php
	} else { // jika statusnya true maka 
		include('pages/main.php'); // tampilkan halaman main.php
	}

?>