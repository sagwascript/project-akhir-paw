<?php

	include('redirect.php'); // include file redirect.php untuk menggunakan fungsi redirect
	
	if (isset($_POST['btnlogout'])) { // jika user menekan tombol logout maka
		// session_start();
		unset($_SESSION['loggedin']); // unset session loggedin

		redirect('index.php'); // redirect ke halaman index
	}

?>