<?php

	if (isset($_POST['btnsimpan'])) {
		
		include 'koneksi.php'; // include file koneksi.php untuk menyambungkan dengan database
		include 'validator.inc'; // include file validator.inc untuk load fungsi validate

		$request = $_POST; // mengisikan variabel request dengan variabel $_POST

		$rules = [ // definisikan rules/aturan untuk tiap kolom isian
			'username' => 'required|min:3|max:20',
			'email' => 'required|email',
			'nama_depan' => 'required|alphabet',
			'nama_belakang' => 'required|alphabet',
			'tanggal_lahir' => 'required',
			'jenis_kelamin' => 'required',
			'alamat' => 'required|min:4'
		];

		$messages = [ // definisikan pesan error untuk setiap aturan
			'required' => 'Isian wajib diisi',
			'alphabet' => 'Isian harus berupa huruf',
			'email' => 'Email tidak valid',
			'min' => 'Panjang @field minimal @size karakter',
			'numeric' => 'Isian harus berupa angka',
			'match' => '@field tidak cocok'
		];

		$errors = validate($request, $rules, $messages); // memanggil fungsi validate		

		if (count($errors) < 1) { // jika variabel error berisi lebih kecil dari 1 maka

			// ambil semua data yang dipostkan oleh user
			$username = $_POST['username'];
			$email = $_POST['email'];
			$nama_depan = $_POST['nama_depan'];
			$nama_belakang = $_POST['nama_belakang'];
			$tanggal_lahir = $_POST['tanggal_lahir'];
			$jenis_kelamin = $_POST['jenis_kelamin'];
			$alamat = $_POST['alamat'];

			// update data pada semua kolom tabel user sesuai dengan yang diinputkan oleh user
			$query = $connection->prepare("UPDATE users SET username = :username, email = :email,nama_depan = :nama_depan, nama_belakang = :nama_belakang, tanggal_lahir = :tanggal_lahir, jenis_kelamin = :jenis_kelamin, alamat = :alamat WHERE username = :user");

			$query->bindValue(':user', $_SESSION['loggedin']['username']); // ganti parameter :user dengan nilai dari session
			$query->bindValue(':username', $username); // ganti parameter :username dengan nilai variabel username
			$query->bindValue(':email', $email); // ganti parameter :email dengan nilai variabel email
			$query->bindValue(':nama_depan', $nama_depan); // ganti parameter :nama_depan dengan nilai variabel nama_depan
			$query->bindValue(':nama_belakang', $nama_belakang); // ganti parameter :nama_belakang dengan nilai variabel nama_belakang
			$query->bindValue(':tanggal_lahir', $tanggal_lahir); // ganti parameter :tanggal_lahir dengan nilai variabel tanggal_lahir
			$query->bindValue(':jenis_kelamin', $jenis_kelamin);  // ganti parameter :jenis_kelamin dengan nilai variabel jenis_kelamin
			$query->bindValue(':alamat', $alamat); // ganti parameter :alamat dengan nilai variabel alamat

			$query->execute(); // eksekusi SQL

			$_SESSION['loggedin']['username'] = $username; // ubah nilai dari session logged in dengan username yang baru diupdate

			redirect('index.php?page=profile'); // redirect ke index.php pada halaman profil
		} 
		else { // jika terdapat error maka
			$_SESSION['edit']['errors'] = $errors; // set session error untuk halaman edit dengan nilai dari variabel errors
			$_SESSION['edit']['status'] = true; // ubah status error menjadi true
			redirect('index.php?page=profile&action=edit'); // redirect ke url untuk mengedit profil
		}
	}

?>