<?php
	
	include 'redirect.php'; // include file redirect.php untuk load fungsi redirect

	if (isset($_POST['btnlogin'])) { // jika user menekan tombol btnlogin
		if (checkpassword($_POST['username'], $_POST['password'])) { // jika username dan password cocok
			
			include 'koneksi.php'; // include koneksi.php untuk menyambungkan database
			include 'validator.inc'; // include validator.inc untuk menggunakan fungsi validate

			$username = $_POST['username']; // set variabel username dengan input user pada field username
			$password = $_POST['password']; // set variabel password dengan input user pada field password

			$rules = [ // definisikan aturan validasinya
				'username' => 'required',
				'password' => 'required'
			];

			$messages = [ // definisikan pesan error jika validasi gagal
				'required' => 'Isian wajib diisi',
			];

			$request = $_POST; // set variabel request dengan nilai dari $_POST

			$errors = validate($request, $rules, $messages); // validasi masukan user sesuai aturan dan pesan errornya

			session_start(); // start session

			$q = $connection->prepare("SELECT * FROM users WHERE username = :username and password = SHA2(:password, 0)"); // cocokkan username dan password yang dimasukkan user dengan username dan password pada database
			$q->bindValue(':username', $username);
			$q->bindValue(':password', $password);
			$q->execute(); // eksekusi SQL

			$res = $q->fetch(); // ambil data pertama dari hasil eksekusi SQL

			if ($q->rowCount() == 1) { // jika hasil eksekusinya terdapat satu baris maka
				$_SESSION['loggedin']['status'] = true; // set status login menjadi true
				$_SESSION['loggedin']['username'] = $res['username']; // berikan nilai pada session username dengan inputan username user
			}
			
		} 

		redirect('index.php'); // redirect ke index.php
	}

	function checkpassword($username, $password) { // definisikan fungsi checkpassword yang menerima dua argumen yaitu username dan password

		include('koneksi.php'); // include koneksi.php untuk menyambungkan database 
		
		$q = $connection->prepare("SELECT * FROM users WHERE username = :username and password = SHA2(:password, 0)"); // cocokkan username dan password yang dimasukkan user dengan username dan password pada database
		$q->bindValue(':username', $username);
		$q->bindValue(':password', $password);
		$q->execute(); // eksekusi SQL

		return $q->rowCount() > 0; // kembalikan nilai true atau false berdasar banyak baris yang didapat dari eksekusi SQL

	}

?>