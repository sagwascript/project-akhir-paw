<?php

	if (isset($_POST['btnregister'])) { // jika user menekan tombol register maka
		
		include 'app/koneksi.php'; // include file koneksi.php untuk menyambungkan dengan database
		include 'app/validator.inc'; // include file validator.inc untuk menggunakan fungsi validate
		include 'app/redirect.php'; // include file redirect.php untuk menggunakan fungsi redirect

		$request = $_POST; // mengisikan variabel request dengan variabel $_POST

		$rules = [ // definisikan rules/aturan untuk tiap kolom isian
			'username' => 'required|min:3|max:20',
			'email' => 'required|email',
			'password' => 'required|min:8',
			're-password' => 'required|match:password',
			'nama_depan' => 'required|alphabet',
			'nama_belakang' => 'required|alphabet',
			'tanggal_lahir' => 'required',
			'jenis_kelamin' => 'required',
			'alamat' => 'required|min:4'
		];

		$messages = [ // definisikan pesan error untuk setiap aturan
			'required' => 'Isian wajib diisi',
			'alphabet' => 'Isian harus berupa huruf',
			'email' => 'Email tidak valid',
			'min' => 'Panjang @field minimal @size karakter',
			'numeric' => 'Isian harus berupa angka',
			'match' => '@field tidak cocok'
		];

		$errors = validate($request, $rules, $messages); // memanggil fungsi validate		

		if (count($errors) < 1) { // jika error lebih kecil dari 1 maka

			// masukkan setiap input user kedalam variabel
			$username = $_POST['username'];
			$email = $_POST['email'];
			$password = $_POST['password'];
			$nama_depan = $_POST['nama_depan'];
			$nama_belakang = $_POST['nama_belakang'];
			$tanggal_lahir = $_POST['tanggal_lahir'];
			$jenis_kelamin = $_POST['jenis_kelamin'];
			$alamat = $_POST['alamat'];

			// konversi string tahun saat ini ke dalam integer
			$year = intval(date("Y"));

			// konversi tahun yang di inputkan oleh user ke integer
			$born = intval(substr($tanggal_lahir,0,4));
			
			// jika tahun saat ini dikurangi dengan tahun lahir user lebih dari 16 maka
			if ($year-$born > 16) {
			
				$q = $connection->prepare("INSERT INTO users VALUES (:username, :email, SHA2(:password, 0), :nama_depan, :nama_belakang, :tanggal_lahir, :jenis_kelamin, :alamat);"); // insert ke dalam tabel users 

				// gantikan setiap parameter yang diisikan pada query dengan nilai pada input oleh user
				$q->bindValue(':username', $username);
				$q->bindValue(':email', $email);
				$q->bindValue(':password', $password);
				$q->bindValue(':nama_depan', $nama_depan);
				$q->bindValue(':nama_belakang', $nama_belakang);
				$q->bindValue(':tanggal_lahir', $tanggal_lahir);
				$q->bindValue(':jenis_kelamin', $jenis_kelamin);
				$q->bindValue(':alamat', $alamat);

				$q->execute(); // eksekusi SQL

				redirect('index.php'); // redirect ke index.php

			} else { // jika umur dibawah 17 tahun maka
				$errors['tahun'] = [ 'age-res' => 'Umur anda dibawah 17 tahun.']; // definisikan error
				$_SESSION['register']['errors'] = $errors; // masukkan error kedalam session
				$_SESSION['register']['status'] = true; // berikan status true pada error
				$_SESSION['register']['data'] = $request; // berikan nilai request pada session data agar field yang sudah benar tidak perlu mengisikan ulang
				redirect('index.php?page=register'); // redirect ke register.php
			}
		} else { // jika tidak lolos validasi maka
			$_SESSION['register']['errors'] = $errors; // masukkan error kedalam session errors
			$_SESSION['register']['status'] = true; // ubah status error menjadi true
			$_SESSION['register']['data'] = $request; // berikan nilai request pada session data agar field yang sudah benar tidak perlu mengisikan ulang
			redirect('index.php?page=register'); // redirect ke register.php
		}
	}

?>