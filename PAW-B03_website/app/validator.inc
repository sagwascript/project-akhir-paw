<?php
	function show_error($err, $field) {
		if (count($err[$field])) {
			echo "<span style='color:red;margin-top:0px;'> *".$err[$field][0]."<span>";
		}
	}

	function show_value($err, $field) {
		if (count($err[$field]) == 0) {
			echo $_POST[$field];
		}
	}

	function cek_error($errors) {

		if ($errors == []) {
			return false;
		}

		foreach ($errors as $err) {
			if (count($err)) {
				return false;
			}
		}
		return true;
	}

	function validate($req, $rules, $messages) {
		
		$errors = []; // array kosong untuk menampung semua error yang ditemukan

		foreach ($rules as $key => $value) {
			
			$rule = explode('|', $value); // memecah string menjadi array

			$arr = []; // array kosong untuk menampung error tiap kolom
			for ($i=0; $i < count($rule); $i++) { 				

				if (substr($rule[$i], 0, 3) == 'min') {
					$size = (int)substr($rule[$i],4, strlen($rule[$i])); //mengambil panjang karakter dari rules
					$rule[$i] = 'min'; // mengganti rule menjadi min agar dapat dikenali oleh switch
				} else if (substr($rule[$i], 0, 5) == 'match') {
					$patt = substr($rule[$i],6, strlen($rule[$i])); // mengambil field yang akan dicocokan dari rules
					$rule[$i] = 'match'; // mengganti rule menjadi match agar dapat dikenali oleh switch
				}

				switch ($rule[$i]) { // cek untuk tiap rule yang ditemukan
					case 'required':
						if (!required($req[$key])) {
							array_push($arr, $messages['required']); // tambahkan pesan error pada elemen array
						}
						break;
					case 'alphabet':
						if (!alphabet($req[$key])) {
							array_push($arr, $messages['alphabet']);	
						}
						break;
					case 'numeric':
						if (!numeric($req[$key])) {
							array_push($arr, $messages['numeric']);	
						}
						break;
					case 'email':
						if (!validate_email($req[$key])) {
							array_push($arr, $messages['email']);		
						}
						break;
					case 'min':
						
						if (!validate_min($req[$key], $size)) {

							$messages_col['min'] = str_replace('@field', $key, $messages['min']); // replace @field dengan nama kolomnya
							$messages_col['min'] = str_replace('@size', $size, $messages_col['min']); // replace @size dengan rules sizenya

							array_push($arr, $messages_col['min']);
						}
						break;
					case 'match':
						if (!validate_match($req[$key], $req[$patt])) {
							$messages_col['match'] = str_replace('@field', $patt, $messages['match']); // replace @field dengan nama kolom pencocokan password
							array_push($arr, $messages_col['match']);	
						}
						break;
				}

			}

			if (count($arr) > 0) { // jika error lebih dari 1
				$errors[$key] = $arr; // push ke array errors untuk setiap error yang terkumpul
			}
		}

		return $errors; // mengembalikan nilai dari array variabel errors
	}

	function required($field) { // fungsi mengecek required
		if ($field == '') { // untuk cek apakah isian kosong atau tidak
			return false;
		}
		return true;
	}

	function numeric($field) { // fungsi cek isian angka
		if (preg_match('/^[0-9]+$/', $field)) { // regexp untuk cek angka
			return true;
		}
		return false;
	}

	function alphabet($field) { // fungsi cek isian huruf
		$field = strtolower($field);
		if (preg_match('/^[a-z]+$/', $field)) { // regexp untuk cek alphabet
			return true;
		}
		return false;	
	}

	function email($field) { // fungsi cek isian email (belum selesai)
		if ($field == 'email') {
			return true;
		}
		return false;
	}

	function validate_min($field, $size) { // fungsi cek isian minimal
		if (strlen($field) < $size) {
			return false;
		} 
		return true;
	}

	function validate_match($field1, $field2) { // fungsi cek isian yang dicocokan dengan isian lain
		if ($field1 == $field2) {
			return true;
		}
		return false;
	}

	function validate_email($field) {

		$pattern = '/^[a-zA-Z0-9]*$/'; // pattern untuk huruf & angka

		$val = $field;
		$jmlAt = substr_count($val,'@'); // menghitung jumlah karakter @ pada email


		// jika jumlah @ adalah 1 maka lanjut ke proses selanjutnya
		if ($jmlAt == 1) {

			// memecah email pada bagian @
			$arrEmail = explode('@', $val);

			// array index 0 adalah local-part
			if (strlen($arrEmail[0]) >= 4) {
				
				// jika jumlah karakter '.' lebih dari 1 maka karakter '_' harus 0
				if (substr_count($arrEmail[0], '.') > 0 && substr_count($arrEmail[0], '_') == 0) {

					$arrNama = explode(".", $arrEmail[0]); // memecah local-part pada tiap titik
					
					for ($i = 0; $i < count($arrNama); $i++) {
						// jika pada setiap string memiliki panjang kurang dari dua maka error
						if (strlen($arrNama[$i]) < 2 || !preg_match($pattern, $arrNama[$i])) {
							return false;			
						}
					}

				// jika jumlah karakter '_' lebih dari 1 maka karakter '.' harus 0
				} else if (substr_count($arrEmail[0], '.') == 0 && substr_count($arrEmail[0], '_') > 0) {

					$arrNama = explode('_', $arrEmail[0]); // memecah local-part pada tiap underscore

					for ($i = 0; $i < count($arrNama); $i++) {
						// jika pada setiap string memiliki panjang kurang dari dua maka error
						if (strlen($arrNama[$i]) < 2 || !preg_match($pattern, $arrNama[$i])) {
							return false;
						}
					}				
				}

			} else {
				return false;
			}

			// array index 1 adalah domain
			if (strlen($arrEmail[1]) > 5) {

				// jika jumlah titik lebih besar dari 0 dan lebih kecil atau sama dengan 4
				if (substr_count($arrEmail[1], '.') <= 4 && substr_count($arrEmail[1], '.') > 0) {

					$arrDomain = explode('.', $arrEmail[1]); // memecah string berdasar titik

					for ($i = 0; $i < count($arrDomain); $i++) {
						// jika panjang string setiap bagian domain lebih kecil dari 2 atau kosong maka error
						if (strlen($arrDomain[$i]) < 2 || trim($arrDomain[$i]) == "" || !(preg_match($pattern, $arrDomain[$i]))) {
							return false;			
						}
					}	
				} else {
					return false;
				}
					
			} else {
				// error jika panjang domain kurang dari 5 karakter
				return false;
			}

		} else {
			// error jumlah @ lebih dari 1
			return false;
		}

		return true;
	}
?>